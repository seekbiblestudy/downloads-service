import argparse from "argparse";

const parser = new argparse.ArgumentParser({
    version: '0.0.1',
    addHelp:true,
    description: 'Downloads service'
  });

parser.addArgument(
    [ '-d', '--dir' ],
    {
      help: 'Downloads directory'
    }
);

parser.addArgument(
    [ '-p', '--port' ],
    {
      help: 'Port to listen on'
    }
);

export default parser.parseArgs();