import express from "express";
import fs from "fs-extra";
import path from "path";

import args from "./args";
const app = express();
const port = +args.port || 4000;

app.get('/:sha256', async (req: express.Request, res: express.Response): Promise<void> =>{
    try {
        const file = await fs.readFile(path.join(args.dir, req.params.sha256 ));
        res.status(200).send(file);
    } catch (err) {
        res.status(404).send();
    }
});

app.post('/', (_: express.Request, res: express.Response): void =>{
    
});

app.listen(port, () => console.log(`Downloads service listening on port ${port}!`))