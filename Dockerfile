FROM node:10 as APPBUILD
COPY src/ /app/src
COPY package.json /app/
COPY package-lock.json /app/
COPY tsconfig.json /app
WORKDIR /app
RUN npm install && npm run build && rm -rf node_modules/
RUN npm install --only=prod
RUN rm -rf src/


FROM node:10
COPY --from=APPBUILD /app /app 
EXPOSE 80
CMD ["node", "/app/dist/main.js", "--dir=/var/downloads", "--port=80"]